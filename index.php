<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="./assets/css/style.css">
</head>
<body class="center">
    <h2>Today is <span class="green"><?php
         date_default_timezone_set('Asia/Makassar');
         $now =  date('l'); echo $now?></span>
    </h2>
    <div class="row">
                <?php
                $days = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
                foreach($days as $day){
                    if($now == $day){
                    echo '<button class="inline bg-green">'.$day.'</button>';
                    } else{
                    echo '<button class="inline bg-blue">'.$day.'</button>';
                    }
                }
                ?>
    </div>
    <h2 id="selectedDay"></h2>
<script src="./assets/js/jquery.min.js"></script>
<script src="./assets/js/main.js"></script>
</body>
</html>